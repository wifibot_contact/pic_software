/* 
 * File:   pid.c
 * Author: rbeal
 *
 * Created on May 9, 2020, 10:40 AM
 */

#include <xc.h>
#include "pid.h"
#include "software_timer.h"

fractional abcCoefficient1[3] __attribute__((space(xmemory)));
fractional controlHistory1[3] __attribute__((space(ymemory), eds));

fractional abcCoefficient2[3] __attribute__((space(xmemory)));
fractional controlHistory2[3] __attribute__((space(ymemory), eds));

fractional abcCoefficient3[3] __attribute__((space(xmemory)));
fractional controlHistory3[3] __attribute__((space(ymemory), eds));

fractional abcCoefficient4[3] __attribute__((space(xmemory)));
fractional controlHistory4[3] __attribute__((space(ymemory), eds));

pid_t pid;

void pid_init(e_pid_id pid_id, u16 p, u16 i, u16 d) {
    
    u8 j;
    
    switch (pid_id) {
        
        case PID_1:
            
            // Init coeff (P.I.D)
            for (j=0; j<3 ;j++) pid.kCoeffs1[j] = 0;
            
            // Set up pointers
            pid.fooPID[0].abcCoefficients = &abcCoefficient1[0];
            pid.fooPID[0].controlHistory  = &controlHistory1[0];
            
            // Clear the controller history and the controller input
            PIDInit(&pid.fooPID[0]);
            
            pid.kCoeffs1[0] = Q15((((float) p) / 100.));
            pid.kCoeffs1[1] = Q15((((float) i) / 100.));
            pid.kCoeffs1[2] = Q15((((float) d) / 100.));
            
            // Derive the coefficients from p,i and d
            PIDCoeffCalc(&pid.kCoeffs1[0], &pid.fooPID[0]);
            
            break;
            
        case PID_2:
            
            // Init coeff (P.I.D)
            for (j=0; j<3 ;j++) pid.kCoeffs2[j] = 0;
            
            // Set up pointers
            pid.fooPID[1].abcCoefficients = &abcCoefficient2[0];
            pid.fooPID[1].controlHistory  = &controlHistory2[0];
            
            // Clear the controller history and the controller input
            PIDInit(&pid.fooPID[1]);
            
            pid.kCoeffs2[0] = Q15((((float) p) / 100.));
            pid.kCoeffs2[1] = Q15((((float) i) / 100.));
            pid.kCoeffs2[2] = Q15((((float) d) / 100.));
            
            // Derive the coefficients from p,i and d
            PIDCoeffCalc(&pid.kCoeffs2[0], &pid.fooPID[1]);
            
            break;
            
        case PID_3:
            
            // Init coeff (P.I.D)
            for (j=0; j<3 ;j++) pid.kCoeffs3[j] = 0;
            
            // Set up pointers
            pid.fooPID[2].abcCoefficients = &abcCoefficient3[0];
            pid.fooPID[2].controlHistory  = &controlHistory3[0];
            
            // Clear the controller history and the controller input
            PIDInit(&pid.fooPID[0]);
            
            pid.kCoeffs3[0] = Q15((((float) p) / 100.));
            pid.kCoeffs3[1] = Q15((((float) i) / 100.));
            pid.kCoeffs3[2] = Q15((((float) d) / 100.));
            
            // Derive the coefficients from p,i and d
            PIDCoeffCalc(&pid.kCoeffs3[0], &pid.fooPID[2]);
            
            break;
            
        case PID_4:
            
            // Init coeff (P.I.D)
            for (j=0; j<3 ;j++) pid.kCoeffs4[j] = 0;
            
            // Set up pointers
            pid.fooPID[3].abcCoefficients = &abcCoefficient4[0];
            pid.fooPID[3].controlHistory  = &controlHistory4[0];
            
            // Clear the controller history and the controller input
            PIDInit(&pid.fooPID[3]);
            
            pid.kCoeffs4[0] = Q15((((float) p) / 100.));
            pid.kCoeffs4[1] = Q15((((float) i) / 100.));
            pid.kCoeffs4[2] = Q15((((float) d) / 100.));
            
            // Derive the coefficients from p,i and d
            PIDCoeffCalc(&pid.kCoeffs4[0], &pid.fooPID[3]);
            
            break;
            
        default:
            // error
            break;
    }
    
    pid.flags.left_enabled  = 1;
    pid.flags.right_enabled = 1;
    
    SoftTimerSetPeriod(eTimerPID, ST_PID_PERIOD);
    SoftTimerStart(eTimerPID);
}

void pid_machine(void) {
    
    if (SoftTimerGetState(eTimerPID) == eTimerRunning)
        return ;

        PID(&pid.fooPID[0]);
        PID(&pid.fooPID[2]);
      
        PID(&pid.fooPID[1]);
        PID(&pid.fooPID[3]);
    
    SoftTimerStart(eTimerPID);
}
