/* 
 * File:   pwm.c
 * Author: rbeal
 *
 * Created on May 8, 2020, 4:28 PM
 */

#include <xc.h>
#include "pwm.h"
#include "pid.h"

pwm_t pwm;

void pwm_init(e_pwm_id pwm_id) {
    
    switch (pwm_id) {
        
        case PWM_1:
            
            IOCON1bits.PENH = 1;     // PWM Module controls PWM1H
            IOCON1bits.POLL = 1;     // PWM1H is active low
            IOCON1bits.PMOD = 0b11;  // PWM I/O pin pair is independent
            PWMCON1bits.CAM = 1;     // Center-Aligned mode is enabled
            
            break;
            
        case PWM_2:
            
            IOCON2bits.PENH = 1;     // PWM Module controls PWM1H
            IOCON2bits.POLL = 1;     // PWM1H is active low
            IOCON2bits.PMOD = 0b11;  // PWM I/O pin pair is independent
            PWMCON2bits.CAM = 1;     // Center-Aligned mode is enabled
            
            break;
            
        case PWM_3:
            
            IOCON3bits.PENH = 1;     // PWM Module controls PWM1H
            IOCON3bits.POLL = 1;     // PWM1H is active low
            IOCON3bits.PMOD = 0b11;  // PWM I/O pin pair is independent
            PWMCON3bits.CAM = 1;     // Center-Aligned mode is enabled
            
            break;
            
        case PWM_4:
            
            IOCON4bits.PENH = 1;     // PWM Module controls PWM1H
            IOCON4bits.POLL = 1;     // PWM1H is active low
            IOCON4bits.PMOD = 0b11;  // PWM I/O pin pair is independent
            PWMCON4bits.CAM = 1;     // Center-Aligned mode is enabled
            
            // TODO : do better
            PTPER = PTERVAL;
            PTCONbits.PTEN = 1; // Enable PWM
            
            break;
            
        default:
            // error
            break;
    }
}

void write_duty(e_pwm_id pwm_id, u16 duty) {
    
    switch (pwm_id) {
        
        case PWM_1:
            
            PDC1 = (duty > PTERVAL) ? PTERVAL : duty;
            break;
            
        case PWM_2:
            
            PDC2 = (duty > PTERVAL) ? PTERVAL : duty;
            break;
            
        case PWM_3:
            
            PDC3 = (duty > PTERVAL) ? PTERVAL : duty;
            break;
            
        case PWM_4:
            
            PDC4 = (duty > PTERVAL) ? PTERVAL : duty;
            break;
            
            default:
                // error
                break;
    }
}

void pwm_machine(void) {
    
    write_duty(PWM_1, pwm.pwm_back_left);
    write_duty(PWM_2, pwm.pwm_back_right);
    write_duty(PWM_3, pwm.pwm_front_left);
    write_duty(PWM_4, pwm.pwm_front_right);
}