/* 
 * File:   ic.h
 * Author: rbeal
 *
 * Created on May 8, 2020, 12:35 PM
 */

#ifndef IC_H
#define	IC_H

#include "types.h"

typedef enum {
    IC_1 = 0,
    IC_2,
    IC_3,
    IC_4,
    IC_5,
    IC_6,
    IC_7,
    IC_8,
    IC_9,
    IC_10,
    IC_11,
    IC_12
} e_ic_id;

typedef enum {
    ODOMETER_FRONT_LEFT = 0,
    ODOMETER_FRONT_RIGHT,
    ODOMETER_BACK_LEFT,
    ODOMETER_BACK_RIGHT
} e_odometers;

typedef struct {
   
    // public
    s32 front_left;
    s32 front_right;
    s32 back_left;
    s32 back_right;
    
    s16 speed_front_left;
    s16 speed_front_right;
    s16 speed_back_left;
    s16 speed_back_right;
    
    //private
    u8 odo_prev_fl;
    u8 odo_prev_fr;
    u8 odo_prev_bl;
    u8 odo_prev_br;
    
    s32 prev_front_left;
    s32 prev_front_right;
    s32 prev_back_left;
    s32 prev_back_right;
    
    
} odometers_t;

extern odometers_t odometers;

void odometer_init(e_odometers odometer_id);
void ic_init(e_ic_id ic_id);
u8 hall_to_dir(u8 current, u8 prev);

void odometry_update(e_odometers odometer_id);
void speed_calculation(e_odometers odometer_id);

#endif	/* IC_H */

