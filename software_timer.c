/*
 * File:   software_timer.c
 * Author: d.gaspar
 *
 * Created on 15 mars 2017, 16:53
 */

// -----------------------------------------------------------------------------
// -------------------------- Header Files Included ----------------------------
// -----------------------------------------------------------------------------

#include <xc.h>
#include "software_timer.h"


// -----------------------------------------------------------------------------
// -------------------------- Global variables ---------------------------------
// -----------------------------------------------------------------------------

tTimerParam SoftTimer[eTimerTotalIds];


// -----------------------------------------------------------------------------
// ------------------------- Private functions ---------------------------------
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
// ------------------------- Public functions ----------------------------------
// -----------------------------------------------------------------------------

/*
 * @brief Init the Software timers
 * @param void
 * @return void
 */
void SoftTimerInit(void)
{
    u8 i;
    
    for(i = 0; i < eTimerTotalIds; i++)
    {
        SoftTimer[i].State = eTimerStopped;
        SoftTimer[i].Time = 0;
    }
}


/*
 * @brief Start the software timer. The timer is resetted before being started.
 * @param eTimerId TimerId: Id of the Timer to be started
 * @return void
 */
void SoftTimerStart(eTimerId TimerId)
{
    if(TimerId < eTimerTotalIds)
    {
        SoftTimer[TimerId].Time = 0;
        SoftTimer[TimerId].State = eTimerRunning;
    }
}


/*
 * @brief Stop the software timer
 * @param eTimerId TimerId: Id of the Timer to be stopped
 * @return void
 */
void SoftTimerStop(eTimerId TimerId)
{
    if(TimerId < eTimerTotalIds)
    {
        SoftTimer[TimerId].State = eTimerStopped;
    }
}


/*
 * @brief Stop the software timer
 * @param eTimerId TimerId: Id of the Timer to be interrogated
 * @return eTimerState: State of the timer
 */
eTimerState SoftTimerGetState(eTimerId TimerId)
{
    eTimerState result = eTimerStopped;
    
    if(TimerId < eTimerTotalIds)
    {
        result = SoftTimer[TimerId].State;
    }
    return result;
}


/*
 * @brief Set the period of the timer
 * @param eTimerId TimerId: Id of the Timer to be configured
 * @param u16 Period: Period of the timer
 * @return void
 */
void SoftTimerSetPeriod(eTimerId TimerId, u16 Period)
{
    if(TimerId < eTimerTotalIds)
    {
        SoftTimer[TimerId].Period = Period;
    }
}


/*
 * @brief Software Timer Callback. Must be called periodically in a
 * @brief Timer interruption.
 * @param void
 * @return void
 */
void SoftTimerCallback(void)
{
    u8 i;
    
    for(i = 0; i < eTimerTotalIds; i++)
    {
        if(SoftTimer[i].State == eTimerRunning)
        {
            SoftTimer[i].Time++;
            if(SoftTimer[i].Time == SoftTimer[i].Period)
            {
                SoftTimer[i].State = eTimerDone;
            }
        }
    }
}
