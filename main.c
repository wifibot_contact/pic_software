/*
 * File:   main.c
 * Author: rbeal
 *
 * Created on May 7, 2020, 8:11 PM
 */


// DSPIC33EP512MC806 Configuration Bit Settings

// FGS
#pragma config GWRP = OFF               // General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF                // General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF               // General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = PRIPLL           // Initial Oscillator Source Selection bits (Primary Oscillator (XT, HS, EC) with PLL)
#pragma config IESO = OFF               // Two-speed Oscillator Start-up Enable bit (Start up device with FRC, then switch to user-selected oscillator source)

// FOSC
#pragma config POSCMD = XT              // Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = ON            // OSC2 Pin Function bit (OSC2 is general purpose digital I/O pin)
#pragma config IOL1WAY = OFF            // Peripheral pin select configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSECMD           // Clock Switching Mode bits (Both Clock switching and Fail-safe Clock Monitor are disabled)

// FWDT
#pragma config WDTPOST = PS32768        // Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128           // Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = OFF             // PLL Lock Wait Enable bit (Clock switch will not wait for the PLL lock signal.)
#pragma config WINDIS = OFF             // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable bit (Watchdog timer enabled/disabled by user software)

// FPOR
#pragma config FPWRT = PWR1             // Power-on Reset Timer Value Select bits (128ms)
#pragma config BOREN = OFF              // Brown-out Reset (BOR) Detection Enable bit (BOR is disabled)
#pragma config ALTI2C1 = OFF            // Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = PGD1               // ICD Communication Channel Select bits (Communicate on PGEC1 and PGED1)
#pragma config RSTPRI = PF              // Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF             // JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF               // Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF                // Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF               // Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)


#include <xc.h>
#include "timers.h"
#include "uart.h"
#include "i2c.h"
#include "ic.h"
#include "pwm.h"
#include "software_timer.h"
#include "adc.h"
#include "pid.h"
#include "motors.h"
#include "sht21.h"
#include "utils.h"
#include "com.h"

void io_init(void);
void clock_init(void);
void pps_init(void);

int main(void) {
    
    clock_init();
    pps_init();
    io_init();
    
    timer_init(TIMER_1); // For software timer
    timer_init(TIMER_2); // For speed calculation
    
    SoftTimerInit();
    
    uart_init(UART_1);
    
    adc_init();
    
    i2c_init();
    sht21_init();
    
    odometer_init(ODOMETER_FRONT_LEFT);
    odometer_init(ODOMETER_FRONT_RIGHT);
    odometer_init(ODOMETER_BACK_LEFT);
    odometer_init(ODOMETER_BACK_RIGHT);
    
    pwm_init(PWM_1);
    pwm_init(PWM_2);
    pwm_init(PWM_3);
    pwm_init(PWM_4);
    
    motor_init();
    
//    SoftTimerSetPeriod(eDebug, (0.5/TIMER_1_PERIOD));
//    SoftTimerStart(eDebug);
    
    /* PID
     *
     * measuredOutput   : values are updated  in ic.c    in speed_calculation
     * controlReference : values are updated  in com.c   in interprete_type_7
     * controlOutput    : values are injected in motor.c in motor_machine
     */
    pid_init(PID_1, 80, 45, 0);
    pid_init(PID_2, 80, 45, 0);
    pid_init(PID_3, 80, 45, 0);
    pid_init(PID_4, 80, 45, 0);
    
    com_init();
        
    INTCON2bits.GIE = 1; // Enable general interrupts
    
    for (;;) {

        adc_machine();
        pid_machine();
        
        motor_machine(MOTOR_FRONT_LEFT);
        motor_machine(MOTOR_FRONT_RIGHT);
        motor_machine(MOTOR_BACK_LEFT);
        motor_machine(MOTOR_BACK_RIGHT);
        pwm_machine();
        
        sht21_machine();
        
        com_parse_machine();
        com_send_machine();
        
        misc_machine();
        watchDog_machine();
    }
    
    return 0;
}

void io_init(void) {
    
    // Analog
    ANSELB = 0x803F; // B15,B5,B4,B3,B2,B1,B0 as analog, others as digital
    ANSELC = 0x0000; // all as digital IO
    ANSELD = 0x0000; // all as digital IO
    ANSELE = 0x0000; // all as digital IO
    ANSELG = 0x0000; // all as digital IO
    
    // Odometers
    TRISDbits.TRISD0 = 1; // A - Coder 1
    TRISDbits.TRISD1 = 1; // B - Coder 1
    TRISDbits.TRISD2 = 1; // C - Coder 1
    TRISDbits.TRISD3 = 1; // A - Coder  2
    TRISDbits.TRISD4 = 1; // B - Coder  2
    TRISDbits.TRISD5 = 1; // C - Coder  2
    TRISFbits.TRISF2 = 1; // A - Coder 3
    TRISFbits.TRISF5 = 1; // B - Coder 3
    TRISFbits.TRISF3 = 1; // C - Coder 3
    TRISFbits.TRISF0 = 1; // A - Coder  4 
    TRISFbits.TRISF1 = 1; // B - Coder  4
    TRISFbits.TRISF4 = 1; // C - Coder  4
    
    // PWM
    TRISEbits.TRISE1 = 0; // PWM 1
    TRISEbits.TRISE3 = 0; // PWM 2
    TRISEbits.TRISE5 = 0; // PWM 3
    TRISEbits.TRISE7 = 0; // PWM 4
    
    // MOTORs DIR
    TRISBbits.TRISB11 = 0; // Motor back left 
    TRISBbits.TRISB14 = 0; // Motor back right
    TRISDbits.TRISD7  = 0; // Motor front left
    TRISDbits.TRISD10 = 0; // Motor front right
    MOTOR_DIR_BACK_LEFT   = 0;
    MOTOR_DIR_BACK_RIGHT  = 0;
    MOTOR_DIR_FRONT_LEFT  = 0;
    MOTOR_DIR_FRONT_RIGHT = 0;
    
    // MOTORs BRAKE
    TRISBbits.TRISB10 = 0; // Brake back left
    TRISBbits.TRISB13 = 0; // Brake back right
    TRISDbits.TRISD6  = 0; // Brake front left
    TRISDbits.TRISD9  = 0; // Brake front left
    MOTOR_BREAK_BACK_LEFT   = 0;
    MOTOR_BREAK_BACK_RIGHT  = 0;
    MOTOR_BREAK_FRONT_LEFT  = 0;
    MOTOR_BREAK_FRONT_RIGHT = 0;
    
    // IOs
    TRISEbits.TRISE2  = 0; // IO1 :
    TRISEbits.TRISE4  = 0; // IO2 : 
    TRISEbits.TRISE6  = 0; // IO3 : 
    TRISBbits.TRISB8  = 0; // IO4 : RPi
    TRISBbits.TRISB9  = 0; // IO5 : client relay
    TRISCbits.TRISC14 = 0; // IO6 : client relay
    TRISEbits.TRISE0  = 0; // IO7 : client relay
    TRISCbits.TRISC13 = 0; // IO_on (general power))
    GENERAL_POWER  = 0;
    UNUSED_1       = 0;
    UNUSED_2       = 0;
    RPI_POWER      = 0;
    CLIENT_RELAY_2 = 0;
    CLIENT_RELAY_3 = 0;
    CLIENT_RELAY_4 = 0;
    GENERAL_POWER  = 1;
    
    // LED
    TRISBbits.TRISB6 = 0; // Charge LED
    LED_CHARGE = 0;
}

void clock_init(void) {
    
    // PRIMARY OSCILLATOR
    
    /*
     *           PLLPRE                PLLPOST     
     *   Xtal ---> N1 ---> VCO ---.----> N2 ----> FOSC
     *                ^           |
     *                ----- M -----
     *                    PLLDIV
     * Xtal = 8MHz
     * FOSC = 120MHz
     * FP   = 60MHz
     * FCY  = 60MHz
     */
    
    CLKDIVbits.PLLPRE   = 0;  // N1=2
    PLLFBDbits.PLLDIV   = 58; // M=60
    CLKDIVbits.PLLPOST  = 0;  // N2=2
    
    __builtin_write_OSCCONH(0x03);          // OSCON.NOSC = XT,HS,EC with PLL
    __builtin_write_OSCCONL(OSCCON | 0x01); // OSCON.OSWEN = 1 (Requests oscillator to switch)

    while (OSCCONbits.COSC != 0b011);   // Waiting for Osc switch
    while (OSCCONbits.LOCK != 1);       // Waiting for PLL lock
}

void pps_init(void) {
    
    __builtin_write_OSCCONL(OSCCON & ~(1<<6));
    
    // Motor Hall 1
    RPINR7bits.IC1R = 64; // RD0
    RPINR7bits.IC2R = 65; // RD1
    RPINR8bits.IC3R = 66; // RD2

    // Motor Hall 2
    RPINR8bits.IC4R = 67; // RD3
    RPINR9bits.IC5R = 68; // RD4
    RPINR9bits.IC6R = 69; // RD5

    // Motor Hall 3
    RPINR10bits.IC7R = 98;  // RF2
    RPINR10bits.IC8R = 101; // RF3
    RPINR33bits.IC9R = 99;  // RF5

    // Motor Hall 4
    RPINR33bits.IC10R = 96;  // RF0
    RPINR34bits.IC11R = 97;  // RF1
    RPINR34bits.IC12R = 100; // RF4

    // UART 1
    RPINR18bits.U1RXR = 118;          // RX : RG6
    RPOR14bits.RP120R = _RPOUT_U1TX ; // TX : RG8
    
    __builtin_write_OSCCONL(OSCCON | (1<<6));
}
