/*
 * File:   uart.c
 * Author: r.beal
 *
 * Created on 2 mai 2018, 10:08
 */

#include <xc.h>
#include <stdio.h>
#include <string.h>
#include "uart.h"

T_SerialBuf bufSerial1Tx;
T_SerialBuf bufSerial1Rx;

T_SerialBuf bufSerial2Tx;
T_SerialBuf bufSerial2Rx;

void uart_init(e_uart_id uart_id) {
    
    switch (uart_id) {
        
        case UART_1:
            
            // @9600 : TODO 19200
            
            U1BRG = BRGVAL_UART_1; // setup uart baudrate
            
            IEC0bits.U1RXIE     = 1; // Enable Rx interrupts
            U1MODEbits.UARTEN   = 1; // Enable uart
            U1STAbits.UTXEN     = 1; // Enable Tx
            
            DELAY_53uS; // Wait at least (1/baudrate) before sending first char
            
            break;
            
        case UART_2:
            
            // @9600
            
            U2BRG = BRGVAL_UART_2; // setup uart baudrate
            
            IEC1bits.U2RXIE     = 1; // Enable Rx interrupts
            U2MODEbits.UARTEN   = 1; // Enable uart
            U2STAbits.UTXEN     = 1; // Enable Tx
            
            DELAY_53uS; // Wait at least (1/baudrate) before sending first char
            
            break;
            
        default:
            // error
            break;
    }
    
    uart_flush(uart_id);
}


void uart_flush(e_uart_id uart_id) {
    
    switch (uart_id) {
        
        case UART_1:
            
            bufSerial1Rx.Nb = 0;
            bufSerial1Rx.CurrentWrite = 0;
            bufSerial1Rx.CurrentRead = 0;
            bufSerial1Tx.Nb = 0;
            bufSerial1Tx.CurrentWrite = 0;
            bufSerial1Tx.CurrentRead = 0;
            break;
            
        case UART_2:
            
            bufSerial2Rx.Nb = 0;
            bufSerial2Rx.CurrentWrite = 0;
            bufSerial2Rx.CurrentRead = 0;
            bufSerial2Tx.Nb = 0;
            bufSerial2Tx.CurrentWrite = 0;
            bufSerial2Tx.CurrentRead = 0;
            break;
            
        default:
            // error
            break;
    }
    

}


u8 uart_read_byte(e_uart_id uart_id,  u8 *data) {
    
    switch (uart_id) {
        
        case UART_1:
            
            if (bufSerial1Rx.Nb == 0)
                return 0; // if circular buffer is empty

            IEC0bits.U1RXIE = 0; // Disable RX interrupt

            *data = bufSerial1Rx.Buf[bufSerial1Rx.CurrentRead];
            bufSerial1Rx.Nb--;

            if (bufSerial1Rx.CurrentRead == MAXBUF -1)
                bufSerial1Rx.CurrentRead = 0;
            else
                bufSerial1Rx.CurrentRead++;

            IEC0bits.U1RXIE = 1; // Enable RX interrupt
            break ;
            
        case UART_2:
            
            if (bufSerial2Rx.Nb == 0)
                return 0; // if circular buffer is empty

            IEC1bits.U2RXIE = 0; // Disable RX interrupt

            *data = bufSerial2Rx.Buf[bufSerial2Rx.CurrentRead];
            bufSerial2Rx.Nb--;

            if (bufSerial2Rx.CurrentRead == MAXBUF -1)
                bufSerial2Rx.CurrentRead = 0;
            else
                bufSerial2Rx.CurrentRead++;

            IEC1bits.U2RXIE = 1; // Enable RX interrupt
            break ;
            
        default:
            // error
            break;
    }
    
    return 1;
}

u8 uart_write_byte(e_uart_id uart_id, u8  data) {
    
    switch (uart_id) {
        
        case UART_1:
            
            Nop();
            
            if (bufSerial1Tx.Nb == MAXBUF)
                return 0; // if circular buffer is empty

            IEC0bits.U1TXIE = 0x00; // Disable TX interrupts

            bufSerial1Tx.Buf[bufSerial1Tx.CurrentWrite] = data;
            bufSerial1Tx.Nb++;

            if (bufSerial1Tx.CurrentWrite == MAXBUF - 1)
                bufSerial1Tx.CurrentWrite = 0;
            else
                bufSerial1Tx.CurrentWrite ++;

            IEC0bits.U1TXIE = 0x01; // Enable TX interrupts
            break;
            
        case UART_2:
            
            if (bufSerial2Tx.Nb == MAXBUF)
                return 0; // if circular buffer is empty

            IEC1bits.U2TXIE = 0x00; // Disable TX interrupts

            bufSerial2Tx.Buf[bufSerial2Tx.CurrentWrite] = data;
            bufSerial2Tx.Nb++;

            if (bufSerial2Tx.CurrentWrite == MAXBUF - 1)
                bufSerial2Tx.CurrentWrite = 0;
            else
                bufSerial2Tx.CurrentWrite ++;

            IEC1bits.U2TXIE = 0x01; // Enable TX interrupts
            break;
            
        default:
            // error
            break;
    }

    return 1;
}

u8 uart_is_all_sent(e_uart_id uart_id) {
    
    switch (uart_id) {
        
        case UART_1:
            
            return ((bufSerial1Tx.Nb == 0) && (U1STAbits.TRMT == 1));
            break;
            
        case UART_2:
            
            return ((bufSerial2Tx.Nb == 0) && (U2STAbits.TRMT == 1));
            break;
            
        default :
            return 0;
            break;
    }
}

void uart_send_int(u32 val) {
    
    char tmp[10];
    u8 i;
    
    sprintf(tmp, "%ld", val);
    
    for (i=0; i<strlen(tmp); i++)
        uart_write_byte(UART_1, tmp[i]);
    
}


// INTERRUPTS ----------

void __attribute__((interrupt, no_auto_psv)) _U1TXInterrupt(void) {
        
    if (bufSerial1Tx.Nb == 0) {
        
        IEC0bits.U1TXIE = 0;
    }
    else {
        
        while (U1STAbits.TRMT == 0) ; // ERRATA !
        
        U1TXREG = bufSerial1Tx.Buf[bufSerial1Tx.CurrentRead];
        IFS0bits.U1TXIF = 0;
        bufSerial1Tx.Nb--;
        
        if (bufSerial1Tx.CurrentRead == MAXBUF - 1)
            bufSerial1Tx.CurrentRead = 0;
        else
            bufSerial1Tx.CurrentRead++;
    }
}

void __attribute__((interrupt, no_auto_psv)) _U1RXInterrupt(void) {
    
    u8 data;
    
    data = U1RXREG;
    IFS0bits.U1RXIF = 0;
    
  	if(bufSerial1Rx.Nb < MAXBUF) {
        
        bufSerial1Rx.Buf[bufSerial1Rx.CurrentWrite] = data;
        bufSerial1Rx.Nb++;
        
        if(bufSerial1Rx.CurrentWrite == MAXBUF - 1)
            bufSerial1Rx.CurrentWrite = 0;
        else
            bufSerial1Rx.CurrentWrite++;
  	}
}

// ---

void __attribute__((interrupt, no_auto_psv)) _U2TXInterrupt(void) {
        
    if (bufSerial2Tx.Nb == 0) {
        
        IEC1bits.U2TXIE = 0;
    }
    else {
        
        while (U2STAbits.TRMT == 0) ; // ERRATA !
        
        U1TXREG = bufSerial2Tx.Buf[bufSerial2Tx.CurrentRead];
        IFS1bits.U2TXIF = 0;
        bufSerial2Tx.Nb--;
        
        if (bufSerial2Tx.CurrentRead == MAXBUF - 1)
            bufSerial2Tx.CurrentRead = 0;
        else
            bufSerial2Tx.CurrentRead++;
    }
}

void __attribute__((interrupt, no_auto_psv)) _U2RXInterrupt(void) {
    
    u8 data;
    
    data = U2RXREG;
    IFS1bits.U2RXIF = 0;
    
  	if(bufSerial2Rx.Nb < MAXBUF) {
        
        bufSerial2Rx.Buf[bufSerial2Rx.CurrentWrite] = data;
        bufSerial2Rx.Nb++;
        
        if(bufSerial2Rx.CurrentWrite == MAXBUF - 1)
            bufSerial2Rx.CurrentWrite = 0;
        else
            bufSerial2Rx.CurrentWrite++;
  	}
}
