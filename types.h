/* 
 * File:   types.h
 * Author: owidar
 *
 */

#ifndef __TYPES_H__
#define __TYPES_H__


#define TRUE    (1)
#define FALSE   (0)

typedef unsigned char uchar;
typedef unsigned int uint;
typedef	unsigned long long	ullong;
typedef signed long long	sllong;

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned long u32;
typedef unsigned long long u64;

typedef union
{
    u16 value;
    u8 bytes[2];
    struct
    {
        u8 low;
        u8 high;
    } byte;
    struct
    {
			u8 q0:4;
			u8 q1:4;
			u8 q2:4;
			u8 q3:4;
    } nibble;
} u16_t;

typedef union
{
    u32 value;
    struct
    {
        u16_t low;
        u16_t high;
    } word;
    u8 bytes[4];
    struct
    {
        u8 b0;
        u8 b1;
        u8 b2;
        u8 b3;
    } byte;
    struct
    {
			u8 q0:4;
			u8 q1:4;
			u8 q2:4;
			u8 q3:4;
			u8 q4:4;
			u8 q5:4;
			u8 q6:4;
			u8 q7:4;
    } nibble;
} u32_t;

typedef union
{
    u64 value;
    struct
    {
        u32_t low;
        u32_t high;
    } dword;
    struct
    {
        u16_t w0;
        u16_t w1;
        u16_t w2;
        u16_t w3;
    } word;
    u8 bytes[8];
    struct
    {
        u8 b0;
        u8 b1;
        u8 b2;
        u8 b3;
        u8 b4;
        u8 b5;
        u8 b6;
        u8 b7;
    } byte;
    struct
    {
			u8 q0:4;
			u8 q1:4;
			u8 q2:4;
			u8 q3:4;
			u8 q4:4;
			u8 q5:4;
			u8 q6:4;
			u8 q7:4;
			u8 q8:4;
			u8 q9:4;
			u8 q10:4;
			u8 q11:4;
			u8 q12:4;
			u8 q13:4;
			u8 q14:4;
			u8 q15:4;
    } nibble;
} u64_t;

typedef signed char s8;
typedef signed short s16;
typedef signed long s32;
typedef signed long long s64;

typedef union
{
    s16 value;
    u8 bytes[2];
    struct
    {
        u8 low;
        u8 high;
    } byte;
    struct
    {
			u8 q0:4;
			u8 q1:4;
			u8 q2:4;
			u8 q3:4;
    } nibble;
} s16_t;

typedef union
{
    s32 value;
    struct
    {
        u16_t low;
        u16_t high;
    } word;
    u8 bytes[4];
    struct
    {
        u8 b0;
        u8 b1;
        u8 b2;
        u8 b3;
    } byte;
    struct
    {
			u8 q0:4;
			u8 q1:4;
			u8 q2:4;
			u8 q3:4;
			u8 q4:4;
			u8 q5:4;
			u8 q6:4;
			u8 q7:4;
    } nibble;
} s32_t;

typedef union
{
    s64 value;
    struct
    {
        u32_t low;
        u32_t high;
    } dword;
    struct
    {
        u16_t w0;
        u16_t w1;
        u16_t w2;
        u16_t w3;
    } word;
    u8 bytes[8];
    struct
    {
        u8 b0;
        u8 b1;
        u8 b2;
        u8 b3;
        u8 b4;
        u8 b5;
        u8 b6;
        u8 b7;
    } byte;
    struct
    {
			u8 q0:4;
			u8 q1:4;
			u8 q2:4;
			u8 q3:4;
			u8 q4:4;
			u8 q5:4;
			u8 q6:4;
			u8 q7:4;
			u8 q8:4;
			u8 q9:4;
			u8 q10:4;
			u8 q11:4;
			u8 q12:4;
			u8 q13:4;
			u8 q14:4;
			u8 q15:4;
    } nibble;
} s64_t;

#endif /* __TYPES_H__ */
