/* 
 * File:   com.h
 * Author: rbeal
 *
 * Created on May 9, 2020, 4:06 PM
 */

#ifndef COM_H
#define	COM_H

#include "types.h"

#define ST_SEND_COM_PERIOD (0.1/TIMER_1_PERIOD)

typedef struct {
    
    u8 current_packet_type; // 0=4, 1=7, 2=9
    u8 data_cnt;
    u8 full_odometry;
    u8 recep_data[50];
} com_t;

typedef enum {
    eOLDCOM_PARSER_INIT = 0,
    eOLDCOM_PARSER_MAGIC_NUMBER,
    eOLDCOM_PARSER_LENGTH,
    eOLDCOM_PARSER_DATA,
    eOLDCOM_PARSER_CRC_LOW,
    eOLDCOM_PARSER_CRC_HIGH
} eOldComParserState;

typedef struct {
    u8 align;
    u8 len;
    u16 speed_front_left;
    u16 speed_front_right;
    u16 speed_back_left;
    u16 speed_back_right;
    union {
        u16 uflags;
        struct {
            unsigned front_right_forward :1;
            unsigned front_left_forward :1;
            unsigned enable_closed_loop :1;
            unsigned align_1 :1;
            unsigned align_2 :1;
            unsigned align_3 :1;
            unsigned align_4 :1;
            unsigned align_5 :1;
            unsigned full_odometry :1;
            unsigned sleep_mode :1;
            unsigned power_5v   :1;
            unsigned relay_2_on :1;
            unsigned relay_3_on :1;
            unsigned relay_4_on :1;
            unsigned back_right_forward :1;
            unsigned back_left_forward :1;
        };
    } flags;
    u16 crc;
} packet_type_12_t;

typedef struct {
    u8 len;
    u8 speed_left;
    u8 speed_right;
    u8 p;
    u8 i;
    u8 d;
    u16 max_speed;
    u16 crc;
} packet_type_9_t;

typedef struct {
    u8 len;
    u16 speed_left;
    u16 speed_right;
    union {
        u8 uflags;
        struct {
            unsigned left_enable_closed_loop :1;
            unsigned left_forward :1;
            unsigned right_enable_closed_loop :1;
            unsigned right_forward :1;
            unsigned relay_4_on :1;
            unsigned relay_3_on :1;
            unsigned relay_2_on :1;
            unsigned unused     :1;
        };
    } flags;
    u16 crc;
} packet_type_7_t;

typedef struct {
    u8 len;
    u8 foo;
    u16 crc;
} packet_type_4_t;

void com_init(void);

void com_send_machine(void);
void com_parse_machine(void);

void com_send_basic(void);
void com_send_full(void);
void com_parse(u8 data);

void interprete_type_4(packet_type_4_t *packet_type_4);
void interprete_type_7(packet_type_7_t *packet_type_7);
void interprete_type_9(packet_type_9_t *packet_type_9);
void interprete_type_12(packet_type_12_t *packet_type_12);

void com_send_and_crc_acc(u8 data);

#endif	/* COM_H */

