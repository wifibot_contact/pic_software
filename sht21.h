/* 
 * File:   sht21.h
 * Author: rbeal
 *
 * Created on May 9, 2020, 2:02 PM
 */

#ifndef SHT21_H
#define	SHT21_H

#include "types.h"

#define ST_SHT21_PERIOD  (1/TIMER_1_PERIOD)

#define SHT21_ADDR        0x80
#define SHT21_GET_T_HOLD  0xE3
#define SHT21_GET_RH_HOLD 0xE5

typedef struct {
    
    s16 temp;
    s16 hygro;
} sht21_t;

extern sht21_t sht21;

void sht21_init(void);
void sht21_machine(void);

#endif	/* SHT21_H */

