/* 
 * File:   sht21.c
 * Author: rbeal
 *
 * Created on May 9, 2020, 2:02 PM
 */


#include "sht21.h"
#include "software_timer.h"
#include "i2c.h"

sht21_t sht21;

void sht21_init(void) {
    
    SoftTimerSetPeriod(eTimerSht21, ST_SHT21_PERIOD);
    i2c_write(SHT21_ADDR, 0xE6, 0x01); // default mode
    
    SoftTimerStart(eTimerSht21);
    
    sht21.temp = 0;
    sht21.hygro  = 0;
}

void sht21_machine(void) {
    
    u8 read_buf[3];
    u16 raw;
    
    if (SoftTimerGetState(eTimerSht21) == eTimerRunning)
        return ;
    
    
    i2c_multi_read(SHT21_ADDR, SHT21_GET_T_HOLD, read_buf, 3);
    raw = (read_buf[0] <<8) | (read_buf[1] & 0xFC);
    
    sht21.temp = ( -46.85 + 175.72 / 65536.0 * raw );
    
    // ---
    
    i2c_multi_read(SHT21_ADDR, SHT21_GET_RH_HOLD, read_buf, 3);
    raw = (read_buf[0] <<8) | (read_buf[1] & 0xFC);
    
    sht21.hygro = ( -6.0 + 125.0 / 65536.0 * raw );
    
    SoftTimerStart(eTimerSht21);
}
