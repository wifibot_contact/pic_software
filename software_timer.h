/*
 * File:   software_timer.h
 * Author: d.gaspar
 *
 * Created on 15 mars 2017, 16:54
 */

#ifndef __SOFTWARE_TIMER_H__
#define	__SOFTWARE_TIMER_H__

// -----------------------------------------------------------------------------
// -------------------------- Header Files Included ----------------------------
// -----------------------------------------------------------------------------

#include <xc.h>
#include "types.h"


// -----------------------------------------------------------------------------
// ---------------------------- Define zone ------------------------------------
// -----------------------------------------------------------------------------

#define TIMER_1_PERIOD (0.001) /* In seconds */

// -----------------------------------------------------------------------------
// ------------------------------- Typedef -------------------------------------
// -----------------------------------------------------------------------------

/**
* @brief Timer states
*/
typedef enum
{
    eTimerDone = 0,
    eTimerStopped,
    eTimerRunning,
    eTimerTotalStates
}eTimerState;


/**
* @brief Timer parameters
*/
typedef struct
{
    u16 Time;
    u16 Period;
    eTimerState State;
}tTimerParam;


/**
* @brief Timer Ids
*/
typedef enum
{
    eTimerI2CTimeout = 0,
    eTimerAdcSampling,
    eTimerSht21,
    eTimerPID,
    eTimerSendCom,
    eWatchDogControl,
    eDebug,
    eTimerTotalIds
}eTimerId;


// -----------------------------------------------------------------------------
// -------------------------- Global variables ---------------------------------
// -----------------------------------------------------------------------------

extern tTimerParam SoftTimer[eTimerTotalIds];

// -----------------------------------------------------------------------------
// ------------------------- Private functions ---------------------------------
// -----------------------------------------------------------------------------



// -----------------------------------------------------------------------------
// ------------------------- Public functions ----------------------------------
// -----------------------------------------------------------------------------

void SoftTimerInit(void);
void SoftTimerCallback(void);
void SoftTimerStart(eTimerId TimerId);
void SoftTimerStop(eTimerId TimerId);
eTimerState SoftTimerGetState(eTimerId TimerId);
void SoftTimerSetPeriod(eTimerId TimerId, u16 Period);


#endif /* __SOFTWARE_TIMER_H__ */