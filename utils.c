/* 
 * File:   utils.c
 * Author: rbeal
 *
 * Created on May 9, 2020, 2:59 PM
 */

#include <xc.h>
#include "utils.h"
#include "adc.h"
#include "pwm.h"
#include "software_timer.h"
#include "pid.h"
#include "motors.h"

crc_t crc;

float lp_filter2(float signal, float *u, float *y) {
    
         const int m=1;  //m = order of denominator of low pass filter

         u[m] = signal;

         y[m] = 0.9608*y[m-1] + 0.0392*u[m-1];   // these coefficients come from a discretized low pass filter with a pole at 2 rad/sec

         u[m-1] = u[m];          // initialize past values for next frame
         y[m-1] = y[m];

         return y[m];
}

void crc16_init(void) {

    crc.crc = 0xFFFF;
}

void crc16_acc(u8 data) {
    
    u8 bit_cnt;
    u8 parity;
    
    crc.crc ^= data; // XOR within data crc and data

    for (bit_cnt = 0; bit_cnt <= 7; bit_cnt++) { // for each bit

        parity = crc.crc;
        crc.crc >>= 1; // crc shift

        if (parity % 2 == 1)
            crc.crc ^= CRC_POLYNOME; // If parity is even : carry
    }
    
}

u16 crc16_get(void) {
    
    return crc.crc;
}


u16 crc16(u8 *Adresse_tab, u8 Taille_max) {

    u16 Crc = 0xFFFF;
    u16 CptOctet = 0;
    u16 CptBit = 0;
    u16 Parity = 0;
    Crc = 0xFFFF;

    for (CptOctet = 0; CptOctet < Taille_max; CptOctet++) {

        Crc ^= *(Adresse_tab + CptOctet); //Ou exculsif entre octet message et CRC

        for (CptBit = 0; CptBit <= 7; CptBit++) { // Mise a 0 du compteur nombre de bits 

            Parity = Crc;

            Crc >>= 1; // D\E9calage a droite du crc

            if (Parity % 2 == 1) Crc ^= CRC_POLYNOME; // Test si nombre impair -> Apres decalage \E0 droite il y aura une retenue
        } // "ou exclusif" entre le CRC et le polynome generateur.
    }
    return (Crc);
}

void misc_machine(void) {
    
    u16 volt_capa = adc.v_power;
    u16 capa;
    
    
    LED_CHARGE = ! I_CHARGE_STATUS;
    
    if (adc.v_power < 160) {
        
        if ( !pwm.pwm_back_left && !pwm.pwm_back_right && !pwm.pwm_front_left && !pwm.pwm_front_right) {
            
            if (volt_capa > 135)
                volt_capa = 135;
            
            if (volt_capa > 110)
                capa = (volt_capa -110) *4; // Calculate capacity from LiFe tension curve
            else
                capa = 1;
        }
        
        adc.capa = lp_filter2((float) capa, &adc.uv_filter_capa[0], &adc.uv_filter_capa[0]);
    }
    else
        adc.capa = 101;

}

void init_watchDog(void) {
    
    SoftTimerSetPeriod(eWatchDogControl, ST_WATCHDOG_CONTROL_PERIOD);
}

void watchDog_machine(void) {

    if (SoftTimerGetState(eWatchDogControl) == eTimerDone) {
        
        pid.fooPID[0].controlOutput = 0;
        pid.fooPID[1].controlOutput = 0;
        pid.fooPID[2].controlOutput = 0;
        pid.fooPID[3].controlOutput = 0;
        
        motor.front_left_without_pid = 0;
        motor.front_right_without_pid = 0;
        motor.back_left_without_pid = 0;
        motor.back_right_without_pid = 0;
    }
}
