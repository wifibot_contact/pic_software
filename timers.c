/* 
 * File:   timers.c
 * Author: rbeal
 *
 * Created on May 8, 2020, 11:53 AM
 */

#include <xc.h>
#include "timers.h"
#include "software_timer.h"
#include "ic.h"

void timer_init(e_timer_id timer_id) {
    
    switch (timer_id) {
        
        case TIMER_1:
            
            // Timer @1ms
            
            T1CONbits.TON   = 0; // Disable Timer
            T1CONbits.TCS   = 0; // Select internal instruction cycle clock
            T1CONbits.TGATE = 0; // Disable Gated Timer mode
            T1CONbits.TCKPS = 0; // Select 1:1 Prescaler
            TMR1 = 0;     // Clear Timer register
            PR1  = 60000; // Load the period value
            IPC0bits.T1IP = 1; // Set Timer 1 Interrupt Priority Level
            IFS0bits.T1IF = 0; // Clear Timer 1 Interrupt Flag
            IEC0bits.T1IE = 1; // Enable Timer 1 Interrupt
            T1CONbits.TON = 1; // Enable Timer
            
            break;
            
        case TIMER_2:
            
            // Timer @50ms
            
            T2CONbits.TON   = 0;    // Disable Timer
            T2CONbits.TCS   = 0;    // Select internal instruction cycle clock
            T2CONbits.TGATE = 0;    // Disable Gated Timer mode
            T2CONbits.TCKPS = 0b10; // Select 1:64 Prescaler
            TMR2 = 0;     // Clear Timer register
            PR2  = 46875; // Load the period value
            IPC1bits.T2IP = 1; // Set Timer 1 Interrupt Priority Level
            IFS0bits.T2IF = 0; // Clear Timer 1 Interrupt Flag
            IEC0bits.T2IE = 1; // Enable Timer 1 Interrupt
            T2CONbits.TON = 1; // Enable Timer
            
            break;
            
        default:
            // error
            break;
    }
}

void __attribute__((__interrupt__, no_auto_psv)) _T1Interrupt(void) {
    
    TMR1 = 0;          // Clear Timer Register
    IFS0bits.T1IF = 0; // Clear Interrupt Flag
    
    SoftTimerCallback();
}

void __attribute__((__interrupt__, no_auto_psv)) _T2Interrupt(void) {
    
    TMR2 = 0;          // Clear Timer Register
    IFS0bits.T2IF = 0; // Clear Interrupt Flag
    
    speed_calculation(ODOMETER_FRONT_LEFT);
    speed_calculation(ODOMETER_FRONT_RIGHT);
    speed_calculation(ODOMETER_BACK_LEFT);
    speed_calculation(ODOMETER_BACK_RIGHT);
}