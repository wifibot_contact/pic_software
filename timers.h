/* 
 * File:   timers.h
 * Author: rbeal
 *
 * Created on May 8, 2020, 11:53 AM
 */

#ifndef TIMERS_H
#define	TIMERS_H

#include "types.h"


typedef enum {
    TIMER_0 = 0,
    TIMER_1,
    TIMER_2,
    TIMER_3
} e_timer_id;

void timer_init(e_timer_id timer_id);

#endif	/* TIMERS_H */

