/* 
 * File:   i2c.h
 * Author: rbeal
 *
 * Created on May 8, 2020, 11:17 AM
 */

#ifndef I2C_H
#define	I2C_H

#include "types.h"

#define FCY 60000000
#define BAUDRATE_I2C_1 100000
#define  BRGVAL_I2C_1  591 // ((1/BAUDRATE_I2C_1 - 120e-9 )/FCY)-2

#define ST_I2C_TIMEOUT  (0.1/TIMER_1_PERIOD)

void i2c_init(void);
void i2c_write(u8 device_addr, u8 addr, u8 data);
u8 i2c_read(u8 device_addr, u8 addr);
void i2c_multi_read(u8 device_addr, u8 addr, u8 *data, u8 len);
void I2C_busy(void);
void wait_ack(void);

#endif	/* I2C_H */

