/* 
 * File:   i2c.c
 * Author: rbeal
 *
 * Created on May 8, 2020, 11:17 AM
 */

#include <xc.h>

#include "software_timer.h"
#include "i2c.h"

void i2c_init(void) {
    
    SoftTimerSetPeriod(eTimerI2CTimeout, ST_I2C_TIMEOUT);
    I2C1BRG = BRGVAL_I2C_1; // 100k
    I2C1CONbits.I2CEN = 1; // config pins + enable I2C
}


void i2c_write(u8 device_addr, u8 addr, u8 data) {

    I2C1CONbits.SEN = 1; // start bit
    I2C_busy();
    
    I2C1TRN = device_addr | 0x00; // write mode
    I2C_busy();
    wait_ack();
    
    I2C1TRN = addr; // addr
    I2C_busy();
    wait_ack();
    
    I2C1TRN = data; // data
    I2C_busy();
    wait_ack();
    
    I2C1CONbits.PEN = 1; // stop bit
    I2C_busy();
}

u8 i2c_read(u8 device_addr, u8 addr) {

    u8 tmp;
    
    I2C1CONbits.SEN = 1; // start bit
    I2C_busy();
    
    I2C1TRN = device_addr | 0x00; // write mode
    I2C_busy();
    wait_ack();
    
    I2C1TRN = addr; // addr
    I2C_busy();
    wait_ack();
    
    I2C1CONbits.PEN = 1; // stop bit
    I2C_busy();
    
    I2C1CONbits.RSEN = 1; // Restart bit
    I2C_busy();
    
    
    I2C1TRN = device_addr | 0x01; // read mode
    I2C_busy();
    wait_ack();
    
    I2C1CONbits.RCEN = 1;
    I2C_busy();
    
    tmp = I2C2RCV;
    I2C1CONbits.ACKDT = 0;
    
    I2C1CONbits.PEN = 1; // stop bit
    I2C_busy();
    
    return tmp;
}

void i2c_multi_read(u8 device_addr, u8 addr, u8 *data, u8 len) {
        
    I2C1CONbits.SEN = 1; // start bit
    I2C_busy();
    
    I2C1TRN = device_addr & 0xFE; // write mode
    I2C_busy();
    wait_ack();
    
    I2C1TRN = addr; // addr
    I2C_busy();
    wait_ack();
    
    I2C1CONbits.PEN = 1; // stop bit
    I2C_busy();
    
    I2C1CONbits.RSEN = 1; // Restart bit
    I2C_busy();
    
    I2C1TRN = device_addr | 0x01; // read mode
    I2C_busy();
    wait_ack();
    
    u8 i;
    for(i = 0; i < len; i++) {
        
        I2C1CONbits.RCEN = 1;
        I2C_busy();

        *(data + i) = I2C1RCV;
        I2C1CONbits.ACKDT = (i < len - 1) ? 0 : 1;
    }
    
    I2C1CONbits.PEN = 1; // stop bit
    I2C_busy();
}

void I2C_busy(void) {
    
    SoftTimerStart(eTimerI2CTimeout);
    
    while(((I2C1CON & 0x001F) || (I2C1STATbits.TRSTAT)) && (SoftTimerGetState(eTimerI2CTimeout) != eTimerDone)); // TODO
}

void wait_ack(void) {
    
    SoftTimerStart(eTimerI2CTimeout);
    
    while (I2C1STATbits.ACKSTAT && (SoftTimerGetState(eTimerI2CTimeout) != eTimerDone)); // TODO
}