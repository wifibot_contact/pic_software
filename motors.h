/* 
 * File:   motors.h
 * Author: rbeal
 *
 * Created on May 9, 2020, 12:51 PM
 */

#ifndef MOTORS_H
#define	MOTORS_H

#include "types.h"

#define MOTOR_DIR_FRONT_LEFT    LATDbits.LATD7
#define MOTOR_DIR_FRONT_RIGHT   LATDbits.LATD10
#define MOTOR_DIR_BACK_LEFT     LATBbits.LATB11
#define MOTOR_DIR_BACK_RIGHT     LATBbits.LATB14
    
#define MOTOR_BREAK_FRONT_LEFT    LATDbits.LATD6
#define MOTOR_BREAK_FRONT_RIGHT   LATDbits.LATD9
#define MOTOR_BREAK_BACK_LEFT     LATBbits.LATB10
#define MOTOR_BREAK_BACK_RIGHT     LATBbits.LATB13

#define DIR_FORWARD     1
#define DIR_BACKWARD    0

#define BREAK           0
#define NOT_BREAK       1

typedef enum {
    
    MOTOR_FRONT_LEFT = 0,
    MOTOR_FRONT_RIGHT,
    MOTOR_BACK_LEFT,
    MOTOR_BACK_RIGHT
} e_motor_id;

typedef struct {
    
    float front_left_without_pid;
    float front_right_without_pid;
    float back_left_without_pid;
    float back_right_without_pid;
} motor_t;

extern motor_t motor;

void motor_init(void);
void motor_machine(e_motor_id motor_id);

#endif	/* MOTORS_H */

