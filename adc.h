/* 
 * File:   adc.h
 * Author: rbeal
 *
 * Created on May 8, 2020, 5:19 PM
 */

#ifndef ADC_H
#define	ADC_H

#include "types.h"

#define ST_ADC_SAMPLING  (0.01/TIMER_1_PERIOD)

typedef struct {
    
    u16 ir_front_left;
    u16 ir_front_right;
    u16 ir_back_left;
    u16 ir_back_right;
    
    u16 v_power;
    u16 i_power;
    u16 temp;
    
    u8 capa;
    
    float uv_filter_v_power[2];
    float uv_filter_i_power[2];
    float uv_filter_capa[2];

} adc_t;

extern adc_t adc;

typedef enum {
    
    eADC_STATE_SAMPLING_AN0 = 0,
    eADC_STATE_CONVERSION_AN0,
    eADC_STATE_SAMPLING_AN1,
    eADC_STATE_CONVERSION_AN1,
    eADC_STATE_SAMPLING_AN2,
    eADC_STATE_CONVERSION_AN2,
    eADC_STATE_SAMPLING_AN3,
    eADC_STATE_CONVERSION_AN3,
    eADC_STATE_SAMPLING_AN4,
    eADC_STATE_CONVERSION_AN4,
    eADC_STATE_SAMPLING_AN5,
    eADC_STATE_CONVERSION_AN5,
    eADC_STATE_SAMPLING_AN15,
    eADC_STATE_CONVERSION_AN15,
    eADC_STATE_END
            
} e_adc_machine_state;

#define DELAY_20uS asm volatile ("REPEAT, #1200"); Nop(); // 20uS delay

void adc_init(void);
void adc_machine(void);

#endif	/* ADC_H */

