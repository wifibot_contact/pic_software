/* 
 * File:   adc.c
 * Author: rbeal
 *
 * Created on May 8, 2020, 5:19 PM
 */


#include <xc.h>
#include "adc.h"
#include "software_timer.h"
#include "utils.h"

adc_t adc;
e_adc_machine_state adc_machine_state;

void adc_init(void) {
    
    SoftTimerSetPeriod(eTimerAdcSampling, ST_ADC_SAMPLING);
    
    AD1CON3bits.ADCS = 0x05; // Conversion clock is 5xTcy
    
    AD1CON1bits.ADON = 1; // ADC Enabled
    
    DELAY_20uS;
    
    adc.uv_filter_v_power[0] = 0;
    adc.uv_filter_v_power[1] = 0;
    
    adc.uv_filter_i_power[0] = 0;
    adc.uv_filter_i_power[1] = 0;
}

void adc_machine(void) {
    
    if (SoftTimerGetState(eTimerAdcSampling) == eTimerRunning)
        return ; // sampling in progress
    
    switch (adc_machine_state) {
        
        case eADC_STATE_SAMPLING_AN0:
            
            AD1CHS0bits.CH0SA = 0; // Select Analog Input ; AN0
            AD1CHS0bits.CH0NA = 0; // Negative input is VREFL
            AD1CON1bits.SAMP = 1;  // Start sampling
            SoftTimerStart(eTimerAdcSampling);
            
            adc_machine_state = eADC_STATE_CONVERSION_AN0;
            break;
            
        case eADC_STATE_CONVERSION_AN0:
            
            if (AD1CON1bits.SAMP)
                AD1CON1bits.SAMP = 0;
            
            if ( ! AD1CON1bits.DONE)
                return ; // conversion in progress
            
            adc.ir_front_right = ADC1BUF0 >>2;
            
            adc_machine_state = eADC_STATE_SAMPLING_AN1;
            break;
            
            
            
        case eADC_STATE_SAMPLING_AN1:
            
            AD1CHS0bits.CH0SA = 1; // Select Analog Input ; AN1
            AD1CHS0bits.CH0NA = 0; // Negative input is VREFL
            AD1CON1bits.SAMP = 1;  // Start sampling
            SoftTimerStart(eTimerAdcSampling);
            
            adc_machine_state = eADC_STATE_CONVERSION_AN1;
            break;
            
        case eADC_STATE_CONVERSION_AN1:
            
            if (AD1CON1bits.SAMP)
                AD1CON1bits.SAMP = 0;
            
            if ( ! AD1CON1bits.DONE)
                return ; // conversion in progress
            
            adc.ir_back_left = ADC1BUF0 >>2;
            
            adc_machine_state = eADC_STATE_SAMPLING_AN2;
            break;
            
            
        case eADC_STATE_SAMPLING_AN2:
            
            AD1CHS0bits.CH0SA = 2; // Select Analog Input ; AN2
            AD1CHS0bits.CH0NA = 0; // Negative input is VREFL
            AD1CON1bits.SAMP = 1;  // Start sampling
            SoftTimerStart(eTimerAdcSampling);
            
            adc_machine_state = eADC_STATE_CONVERSION_AN2;
            break;
            
        case eADC_STATE_CONVERSION_AN2:
            
            if (AD1CON1bits.SAMP)
                AD1CON1bits.SAMP = 0;
            
            if ( ! AD1CON1bits.DONE)
                return ; // conversion in progress
            
            adc.v_power = (u16) lp_filter2((float) (ADC1BUF0 >>2), &adc.uv_filter_v_power[0], &adc.uv_filter_v_power[1]);
            
            if (adc.v_power < 160) {
                
            }
            
            adc_machine_state = eADC_STATE_SAMPLING_AN3;
            break;
            
            
        case eADC_STATE_SAMPLING_AN3:
            
            AD1CHS0bits.CH0SA = 3; // Select Analog Input ; AN3
            AD1CHS0bits.CH0NA = 0; // Negative input is VREFL
            AD1CON1bits.SAMP = 1;  // Start sampling
            SoftTimerStart(eTimerAdcSampling);
            
            adc_machine_state = eADC_STATE_CONVERSION_AN3;
            break;
            
        case eADC_STATE_CONVERSION_AN3:
            
            if (AD1CON1bits.SAMP)
                AD1CON1bits.SAMP = 0;
            
            if ( ! AD1CON1bits.DONE)
                return ; // conversion in progress
            
            adc.ir_front_left = ADC1BUF0 >>2;
            
            adc_machine_state = eADC_STATE_SAMPLING_AN4;
            break;
            
            
        case eADC_STATE_SAMPLING_AN4:
            
            AD1CHS0bits.CH0SA = 4; // Select Analog Input ; AN4
            AD1CHS0bits.CH0NA = 0; // Negative input is VREFL
            AD1CON1bits.SAMP = 1;  // Start sampling
            SoftTimerStart(eTimerAdcSampling);
            
            adc_machine_state = eADC_STATE_CONVERSION_AN4;
            break;
            
        case eADC_STATE_CONVERSION_AN4:
            
            if (AD1CON1bits.SAMP)
                AD1CON1bits.SAMP = 0;
            
            if ( ! AD1CON1bits.DONE)
                return ; // conversion in progress
            
            adc.ir_back_right = ADC1BUF0 >>2;
            
            adc_machine_state = eADC_STATE_SAMPLING_AN5;
            break;
            
            
        case eADC_STATE_SAMPLING_AN5:
            
            AD1CHS0bits.CH0SA = 5; // Select Analog Input ; AN5
            AD1CHS0bits.CH0NA = 0; // Negative input is VREFL
            AD1CON1bits.SAMP = 1;  // Start sampling
            SoftTimerStart(eTimerAdcSampling);
            
            adc_machine_state = eADC_STATE_CONVERSION_AN5;
            break;
            
        case eADC_STATE_CONVERSION_AN5:
            
            if (AD1CON1bits.SAMP)
                AD1CON1bits.SAMP = 0;
            
            if ( ! AD1CON1bits.DONE)
                return ; // conversion in progress
            
            adc.i_power = (u16) lp_filter2((float) (ADC1BUF0 >>2), &adc.uv_filter_i_power[0], &adc.uv_filter_i_power[1]);
            
            adc_machine_state = eADC_STATE_SAMPLING_AN15;
            break;
            
            
        case eADC_STATE_SAMPLING_AN15:
            
            AD1CHS0bits.CH0SA = 15; // Select Analog Input ; AN15
            AD1CHS0bits.CH0NA = 0; // Negative input is VREFL
            AD1CON1bits.SAMP = 1;  // Start sampling
            SoftTimerStart(eTimerAdcSampling);
            
            adc_machine_state = eADC_STATE_CONVERSION_AN15;
            break;
            
        case eADC_STATE_CONVERSION_AN15:
            
            if (AD1CON1bits.SAMP)
                AD1CON1bits.SAMP = 0;
            
            if ( ! AD1CON1bits.DONE)
                return ; // conversion in progress
            
            adc.temp = ADC1BUF0;
            
            adc_machine_state = eADC_STATE_SAMPLING_AN0;
            break;
            
        default:
            adc_machine_state = eADC_STATE_SAMPLING_AN0;
            break;
    }
}
