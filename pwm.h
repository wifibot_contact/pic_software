/* 
 * File:   pwm.h
 * Author: rbeal
 *
 * Created on May 8, 2020, 4:28 PM
 */

#ifndef PWM_H
#define	PWM_H

#include "types.h"

#define FOSC        120000000
#define PWM_FREQ    24000
#define PTERVAL     (FOSC/PWM_FREQ)

typedef struct {
    u16 pwm_front_left;
    u16 pwm_front_right;
    u16 pwm_back_left;
    u16 pwm_back_right;
} pwm_t;

typedef enum {
    PWM_1 = 0, // BACK LEFT
    PWM_2,     // BACK RIGHT
    PWM_3,     // FRONT LEFT
    PWM_4      // FRONT RIGHT
} e_pwm_id;

extern pwm_t pwm;

void pwm_init(e_pwm_id pwm_id);
void write_duty(e_pwm_id pwm_id, u16 duty);
void pwm_machine(void);

#endif	/* PWM_H */
