/* 
 * File:   utils.h
 * Author: rbeal
 *
 * Created on May 9, 2020, 2:59 PM
 */

#ifndef UTILS_H
#define	UTILS_H

#include "types.h"

#define GENERAL_POWER   LATCbits.LATC13

#define UNUSED_1        LATEbits.LATE4
#define UNUSED_2        LATEbits.LATE6

#define RPI_POWER       LATBbits.LATB8

#define CLIENT_RELAY_2  LATBbits.LATB9
#define CLIENT_RELAY_3  LATCbits.LATC14
#define CLIENT_RELAY_4  LATEbits.LATE0

#define I_CHARGE_STATUS PORTBbits.RB7
#define LED_CHARGE      LATBbits.LATB6

#define CRC_POLYNOME    0xA001

#define ST_WATCHDOG_CONTROL_PERIOD (0.5/TIMER_1_PERIOD)

typedef struct {
    
    u16 crc;
} crc_t;


float lp_filter2(float signal, float *u, float *y);

void crc16_init(void);
void crc16_acc(u8 data);
u16  crc16_get(void);
u16  crc16(u8 *Adresse_tab, u8 Taille_max);

void misc_machine(void);

void init_watchDog(void);
void watchDog_machine(void);

void traps(u8 id);

#endif	/* UTILS_H */

