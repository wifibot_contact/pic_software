/* 
 * File:   pid.h
 * Author: rbeal
 *
 * Created on May 9, 2020, 10:40 AM
 */

#ifndef PID_H
#define	PID_H

#include <dsp.h>
#include "types.h"

#define ST_PID_PERIOD  (0.05/TIMER_1_PERIOD)

typedef struct {
    
    fractional kCoeffs1[3];
    fractional kCoeffs2[3];
    fractional kCoeffs3[3];
    fractional kCoeffs4[3];
    
    tPID fooPID[4];
    
    union {
        u8 uuflags;
        struct {
            unsigned left_enabled  :1;
            unsigned right_enabled :1;
        };
    } flags;
    
} pid_t;

typedef enum {
    
    PID_1 = 0,
    PID_2,
    PID_3,
    PID_4
} e_pid_id;

extern pid_t pid;

void pid_init(e_pid_id pid_id, u16 p, u16 i, u16 d);
void pid_machine(void);

#endif	/* PID_H */

