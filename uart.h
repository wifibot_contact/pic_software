/* 
 * File:   uart.h
 * Author: rbeal
 *
 * Created on May 7, 2020, 10:02 PM
 */

#ifndef UART_H
#define	UART_H

#include "types.h"

#define FCY 60000000
#define DELAY_53uS asm volatile ("REPEAT, #3180"); Nop(); // 105uS delay


#define BAUDRATE_UART_1 19200
#define BRGVAL_UART_1 ((FCY/BAUDRATE_UART_1)/16)-1

#define BAUDRATE_UART_2 9600
#define BRGVAL_UART_2 ((FCY/BAUDRATE_UART_2)/16)-1


#define MAXBUF (256)

typedef struct 
{
    u8 Buf[MAXBUF];
    u8 CurrentRead;
    u8 CurrentWrite;
    u8 Nb;
} T_SerialBuf;


typedef enum {
    UART_1 = 0,
    UART_2
} e_uart_id;

void uart_init(e_uart_id uart_id);
void uart_flush(e_uart_id uart_id);
u8 uart_read_byte(e_uart_id uart_id, u8 *data);
u8 uart_write_byte(e_uart_id uart_id, u8  data);
u8 uart_is_all_sent(e_uart_id uart_id);

void uart_send_int(u32 val);

#endif	/* UART_H */

