/* 
 * File:   motors.c
 * Author: rbeal
 *
 * Created on May 9, 2020, 12:51 PM
 */

#include <xc.h>
#include "motors.h"
#include "pid.h"
#include "pwm.h"

motor_t motor;

void motor_init(void) {
    
    
}

void motor_machine(e_motor_id motor_id) {
    
    s16 control;
    
    switch (motor_id) {
        
        case MOTOR_FRONT_LEFT:
            
            if (pid.flags.left_enabled) {
                // Get control value from PID
                control = Fract2Float(pid.fooPID[2].controlOutput) *PTERVAL;
            }
            else {
                // Get control from raw data
                control = motor.front_left_without_pid *PTERVAL;
            }
            
            // Set direction to BLDC driver
            MOTOR_DIR_FRONT_LEFT   = (control >= 0) ? DIR_FORWARD : DIR_BACKWARD;
            
            // Set break to BLDC driver
            MOTOR_BREAK_FRONT_LEFT = (control == 0) ? BREAK : NOT_BREAK;
            
            // Set duty cycle to PWM driver
            pwm.pwm_front_left = PTERVAL - abs(control);
                
            break;
            
        case MOTOR_FRONT_RIGHT:
            
            if (pid.flags.right_enabled) {
                // Get control value from PID
                control = Fract2Float(pid.fooPID[3].controlOutput) *PTERVAL;
            }
            else {
                // Get control from raw data
                control = motor.front_right_without_pid *PTERVAL;
            }
            
            // Set direction to BLDC driver
            MOTOR_DIR_FRONT_RIGHT   = (control < 0) ? DIR_FORWARD : DIR_BACKWARD;
            
            // Set break to BLDC driver
            MOTOR_BREAK_FRONT_RIGHT = (control == 0) ? BREAK : NOT_BREAK;
            
            // Set duty cycle to PWM driver
            pwm.pwm_front_right = PTERVAL - abs(control);
                
            break;

        case MOTOR_BACK_LEFT:
            
            if (pid.flags.left_enabled) {
                // Get control value from PID
                control = Fract2Float(pid.fooPID[0].controlOutput) *PTERVAL;
            }
            else {
                // Get control from raw data
                control = motor.back_left_without_pid *PTERVAL;
            }
            
            // Set direction to BLDC driver
            MOTOR_DIR_BACK_LEFT   = (control >= 0) ? DIR_FORWARD : DIR_BACKWARD;
            
            // Set break to BLDC driver
            MOTOR_BREAK_BACK_LEFT = (control == 0) ? BREAK : NOT_BREAK;
            
            // Set duty cycle to PWM driver
            pwm.pwm_back_left = PTERVAL - abs(control);
                
            break;
            
        case MOTOR_BACK_RIGHT:
            
            if (pid.flags.right_enabled) {
                // Get control value from PID
                control = Fract2Float(pid.fooPID[1].controlOutput) *PTERVAL;
            }
            else {
                // Get control from raw data
                control = motor.back_right_without_pid *PTERVAL;
            }
            
            // Set direction to BLDC driver
            MOTOR_DIR_BACK_RIGHT   = (control < 0) ? DIR_FORWARD : DIR_BACKWARD;

            // Set break to BLDC driver
            MOTOR_BREAK_BACK_RIGHT = (control == 0) ? BREAK : NOT_BREAK;
            
            // Set duty cycle to PWM driver
            pwm.pwm_back_right = PTERVAL - abs(control);
                
            break;
            
        default :
            // error
            break;
    }
}