/* 
 * File:   com.c
 * Author: rbeal
 *
 * Created on May 9, 2020, 4:06 PM
 */
#include <string.h>
#include "com.h"
#include "uart.h"
#include "ic.h"
#include "software_timer.h"
#include "adc.h"
#include "utils.h"
#include "pid.h"
#include "motors.h"
#include "sht21.h"

com_t com;
eOldComParserState old_com_parser_state;

packet_type_4_t  packet_type_4;
packet_type_7_t  packet_type_7;
packet_type_9_t  packet_type_9;
packet_type_12_t packet_type_12;

void com_init(void) {
    
    old_com_parser_state = eOLDCOM_PARSER_INIT;
    
    SoftTimerSetPeriod(eTimerSendCom, ST_SEND_COM_PERIOD);
    SoftTimerStart(eTimerSendCom);
}

void com_send_machine(void) {
    
    if(SoftTimerGetState(eTimerSendCom) == eTimerRunning)
        return ;
    
    if (com.full_odometry)
        com_send_full();
    else
        com_send_basic();
    
    SoftTimerStart(eTimerSendCom);
}

void com_parse_machine(void) {
    
    u8 data;
    
    if (uart_read_byte(UART_1, &data) == 0)
        return ;
    
    com_parse(data);
}

void com_parse(u8 data) {
    
    u16 crc_16;
    
    switch (old_com_parser_state) {
        
        case eOLDCOM_PARSER_INIT:
            
            memset(&packet_type_4, 0, sizeof(packet_type_4_t));
            memset(&packet_type_7, 0, sizeof(packet_type_7_t));
            memset(&packet_type_9, 0, sizeof(packet_type_9_t));
            memset(&packet_type_12, 0, sizeof(packet_type_12_t));
            com.current_packet_type = 0;
            
            old_com_parser_state = eOLDCOM_PARSER_MAGIC_NUMBER;
            break;
            
        case eOLDCOM_PARSER_MAGIC_NUMBER:
            
            if (data == 0xFF)
                old_com_parser_state = eOLDCOM_PARSER_LENGTH;
            break;
            
        case eOLDCOM_PARSER_LENGTH:
            
            if (data == 4 || data == 7 || data == 9 || data == 12) {
                com.current_packet_type = data;
                com.data_cnt = 0;
                
             if (com.current_packet_type == 4)
                packet_type_4.len = data;
            
            if (com.current_packet_type == 7)
                packet_type_7.len = data;
            
            if (com.current_packet_type == 9)
                packet_type_9.len = data;
                
            if (com.current_packet_type == 12)
                packet_type_12.len = data;
                
                old_com_parser_state = eOLDCOM_PARSER_DATA;
            }
            
            break;
            
        case eOLDCOM_PARSER_DATA:
            
            if (com.current_packet_type == 4)
                *(&packet_type_4.len +1 + com.data_cnt++) = data;
            
            if (com.current_packet_type == 7)
                *(&packet_type_7.len +1 + com.data_cnt++) = data;
            
            if (com.current_packet_type == 9)
                *(&packet_type_9.len +1 + com.data_cnt++) = data;

            if (com.current_packet_type == 12)
                *(&packet_type_12.len +1 + com.data_cnt++) = data;
            
            if (com.data_cnt == com.current_packet_type -2)
                old_com_parser_state = eOLDCOM_PARSER_CRC_LOW;
            
            break;
            
        case eOLDCOM_PARSER_CRC_LOW:
            
            if (com.current_packet_type == 4)
                packet_type_4.crc = data & 0x00FF;
            
            if (com.current_packet_type == 7)
                packet_type_7.crc = data & 0x00FF;
            
            if (com.current_packet_type == 9)
                packet_type_9.crc = data & 0x00FF;
            
            if (com.current_packet_type == 12)
                packet_type_12.crc = data & 0x00FF;
            
            old_com_parser_state = eOLDCOM_PARSER_CRC_HIGH;
            
            break;
            
        case eOLDCOM_PARSER_CRC_HIGH:
            
            if (com.current_packet_type == 4) {
                packet_type_4.crc |= data <<8;
                crc_16 = crc16( (u8 *) &packet_type_4, 4 -2);
                
                if (crc_16 == packet_type_4.crc)
                    interprete_type_4(&packet_type_4);
            }
            
            if (com.current_packet_type == 7) {
                packet_type_7.crc |= data <<8;
                crc_16 = crc16( (u8 *) &packet_type_7, 7 -2);
                
                if (crc_16 == packet_type_7.crc)
                    interprete_type_7(&packet_type_7);
            }
            
            if (com.current_packet_type == 9) {
                packet_type_9.crc |= data <<8;
                crc_16 = crc16( (u8 *) &packet_type_9, 9 -2);
                
                if (crc_16 == packet_type_9.crc)
                    interprete_type_9(&packet_type_9);
            }
            
            if (com.current_packet_type == 12) {
                packet_type_12.crc |= (u16)data <<8;
                crc_16 = crc16( (u8 *) &packet_type_12 +1, 11);
                
                if (crc_16 == packet_type_12.crc)
                    interprete_type_12(&packet_type_12);
            }
            
            old_com_parser_state = eOLDCOM_PARSER_MAGIC_NUMBER;
            
            break;
    }
    
}

void com_send_basic(void) {
    
    u16 crc16;

    com_send_and_crc_acc(0xFF); // Magic number
    
    crc16_init();
    
    com_send_and_crc_acc((odometers.speed_front_left * 5)      & 0xFF);
    com_send_and_crc_acc(((odometers.speed_front_left *5) >>8) & 0xFF);
    
    com_send_and_crc_acc(adc.v_power & 0xFF);
    
    com_send_and_crc_acc(adc.ir_front_left  & 0xFF);
    com_send_and_crc_acc(adc.ir_front_right & 0xFF);
    
    com_send_and_crc_acc( odometers.front_left       & 0xFF);
    com_send_and_crc_acc((odometers.front_left >>8 ) & 0xFF);
    com_send_and_crc_acc((odometers.front_left >>16) & 0xFF);
    com_send_and_crc_acc((odometers.front_left >>24) & 0xFF);
    
    com_send_and_crc_acc((odometers.speed_front_right * 5)      & 0xFF);
    com_send_and_crc_acc(((odometers.speed_front_right *5) >>8) & 0xFF);
    
    
    com_send_and_crc_acc(adc.ir_front_right & 0xFF);
    com_send_and_crc_acc(adc.ir_back_left   & 0xFF);
    
    com_send_and_crc_acc( odometers.front_right       & 0xFF);
    com_send_and_crc_acc((odometers.front_right >>8 ) & 0xFF);
    com_send_and_crc_acc((odometers.front_right >>16) & 0xFF);
    com_send_and_crc_acc((odometers.front_right >>24) & 0xFF);
    
    com_send_and_crc_acc(adc.i_power & 0xFF);
    com_send_and_crc_acc(adc.capa & 0xFF);
    
    crc16 = crc16_get();
    
    uart_write_byte(UART_1,  crc16     & 0xFF);
    uart_write_byte(UART_1, (crc16>>8) & 0xFF);
}

void com_send_full(void) {
    
    u16 crc16;

    com_send_and_crc_acc(0xFE); // Magic number
    
    crc16_init();
    
    com_send_and_crc_acc(31); // size
    
    com_send_and_crc_acc((odometers.speed_front_left * 5)      & 0xFF);
    com_send_and_crc_acc(((odometers.speed_front_left *5) >>8) & 0xFF);
    
    com_send_and_crc_acc(adc.v_power & 0xFF);
    
    com_send_and_crc_acc(adc.ir_front_left  & 0xFF);
    com_send_and_crc_acc(adc.ir_front_right & 0xFF);
    
    com_send_and_crc_acc( odometers.front_left       & 0xFF);
    com_send_and_crc_acc((odometers.front_left >>8 ) & 0xFF);
    com_send_and_crc_acc((odometers.front_left >>16) & 0xFF);
    com_send_and_crc_acc((odometers.front_left >>24) & 0xFF);
    
    com_send_and_crc_acc((odometers.speed_front_right * 5)      & 0xFF);
    com_send_and_crc_acc(((odometers.speed_front_right *5) >>8) & 0xFF);
    
    com_send_and_crc_acc(adc.ir_back_right & 0xFF);
    com_send_and_crc_acc(adc.ir_back_left   & 0xFF);
    
    com_send_and_crc_acc( odometers.front_right       & 0xFF);
    com_send_and_crc_acc((odometers.front_right >>8 ) & 0xFF);
    com_send_and_crc_acc((odometers.front_right >>16) & 0xFF);
    com_send_and_crc_acc((odometers.front_right >>24) & 0xFF);
    
    com_send_and_crc_acc( odometers.back_left       & 0xFF);
    com_send_and_crc_acc((odometers.back_left >>8 ) & 0xFF);
    com_send_and_crc_acc((odometers.back_left >>16) & 0xFF);
    com_send_and_crc_acc((odometers.back_left >>24) & 0xFF);

    com_send_and_crc_acc( odometers.back_right       & 0xFF);
    com_send_and_crc_acc((odometers.back_right >>8 ) & 0xFF);
    com_send_and_crc_acc((odometers.back_right >>16) & 0xFF);
    com_send_and_crc_acc((odometers.back_right >>24) & 0xFF);
    
    com_send_and_crc_acc(adc.i_power & 0xFF);
    com_send_and_crc_acc(adc.capa & 0xFF);
    
    com_send_and_crc_acc(sht21.temp & 0xFF);
    com_send_and_crc_acc(sht21.hygro & 0xFF);
    
    crc16 = crc16_get();
    
    uart_write_byte(UART_1,  crc16     & 0xFF);
    uart_write_byte(UART_1, (crc16>>8) & 0xFF);
}

void com_send_modbus(void) {
    
    
}

void interprete_type_4(packet_type_4_t *packet_type_4) {
    
    (void) packet_type_4;
    // TODO : watchdog
}

void interprete_type_7(packet_type_7_t *packet_type_7) {
    
    if (packet_type_7->flags.left_enable_closed_loop) {
        
        pid.fooPID[0].controlReference = Q15((float) packet_type_7->speed_left / 250.f);
        pid.fooPID[2].controlReference = Q15((float) packet_type_7->speed_left / 250.f);
        
    }
    else {
        motor.front_left_without_pid = (float) packet_type_7->speed_left / 65535.f;
        motor.back_left_without_pid  = (float) packet_type_7->speed_left / 65535.f;
    }
    
    if (packet_type_7->flags.right_enable_closed_loop) {
        
        pid.fooPID[1].controlReference = Q15((float) packet_type_7->speed_right / 250.f);
        pid.fooPID[3].controlReference = Q15((float) packet_type_7->speed_right / 250.f);
        
    }
    else {
        motor.front_right_without_pid = (float) packet_type_7->speed_right / 65535.f;
        motor.back_right_without_pid  = (float) packet_type_7->speed_right / 65535.f;
    }
    
    pid.flags.left_enabled  = packet_type_7->flags.left_enable_closed_loop;
    pid.flags.right_enabled = packet_type_7->flags.right_enable_closed_loop;
    
    CLIENT_RELAY_4 =   packet_type_7->flags.relay_4_on;
    CLIENT_RELAY_3 =   packet_type_7->flags.relay_3_on;
    CLIENT_RELAY_2 =   packet_type_7->flags.relay_2_on;
    (void) packet_type_7->flags.unused;
    
    SoftTimerStart(eWatchDogControl); // watchdog reset
}

void interprete_type_9(packet_type_9_t *packet_type_9) {
    
    (void) packet_type_9->speed_left;  // always 0
    (void) packet_type_9->speed_right; // always 0
    
    pid_init(PID_1, packet_type_9->p, packet_type_9->i, packet_type_9->d);
    pid_init(PID_2, packet_type_9->p, packet_type_9->i, packet_type_9->d);
    pid_init(PID_3, packet_type_9->p, packet_type_9->i, packet_type_9->d);
    pid_init(PID_4, packet_type_9->p, packet_type_9->i, packet_type_9->d);
    
    // max speed : TODO
}

void interprete_type_12(packet_type_12_t *packet_type_12) {
    
    if (packet_type_12->flags.enable_closed_loop) {
        
        pid.fooPID[0].controlReference = (packet_type_12->flags.front_left_forward) ? Q15((float) packet_type_12->speed_front_left  / 32768.f) : Q15((float) - packet_type_12->speed_front_left  / 32768.f) ;
        pid.fooPID[1].controlReference = (packet_type_12->flags.front_right_forward) ? Q15((float) packet_type_12->speed_front_right  / 32768.f) : Q15((float) - packet_type_12->speed_front_right  / 32768.f) ;
        pid.fooPID[2].controlReference = (packet_type_12->flags.back_left_forward) ? Q15((float) packet_type_12->speed_back_left  / 32768.f) : Q15((float) - packet_type_12->speed_back_left  / 32768.f) ;
        pid.fooPID[3].controlReference = (packet_type_12->flags.back_right_forward) ? Q15((float) packet_type_12->speed_back_right  / 32768.f) : Q15((float) - packet_type_12->speed_back_right  / 32768.f) ;
    }
    else {
        
        motor.front_left_without_pid  = (packet_type_12->flags.front_left_forward) ? (float) packet_type_12->speed_front_left  / 32768.f : (float) - packet_type_12->speed_front_left  / 32768.f;
        motor.back_left_without_pid  = (packet_type_12->flags.back_left_forward) ? (float) packet_type_12->speed_front_right  / 32768.f : (float) - packet_type_12->speed_front_right  / 32768.f;
        motor.front_right_without_pid  = (packet_type_12->flags.front_right_forward) ? (float) packet_type_12->speed_back_left  / 32768.f : (float) - packet_type_12->speed_back_left  / 32768.f;
        motor.back_right_without_pid  = (packet_type_12->flags.back_right_forward) ? (float) packet_type_12->speed_back_right  / 32768.f : (float) - packet_type_12->speed_back_right  / 32768.f;
    }
    
    pid.flags.left_enabled  = packet_type_12->flags.enable_closed_loop;
    pid.flags.right_enabled = packet_type_12->flags.enable_closed_loop;
    
    CLIENT_RELAY_4    = packet_type_12->flags.relay_4_on;
    CLIENT_RELAY_3    = packet_type_12->flags.relay_3_on;
    CLIENT_RELAY_2    = packet_type_12->flags.relay_2_on;
    RPI_POWER         = packet_type_12->flags.power_5v;
    GENERAL_POWER     = packet_type_12->flags.sleep_mode;
    com.full_odometry = packet_type_12->flags.full_odometry;
    
    SoftTimerStart(eWatchDogControl); // watchdog reset
}

void com_send_and_crc_acc(u8 data) {
    
    uart_write_byte(UART_1, data);
    crc16_acc(data);
}