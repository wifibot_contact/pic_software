/* 
 * File:   ic.c
 * Author: rbeal
 *
 * Created on May 8, 2020, 12:35 PM
 */

#include <xc.h>
#include "ic.h"
#include "pid.h"

odometers_t odometers;

void ic_init(e_ic_id ic_id) {
    
    switch (ic_id) {
        
        case IC_1:
            
            IC1CON1bits.ICM    = 0b001; // Capture mode : every edge
            IC1CON1bits.ICTSEL = 0b000; // Select Timer 3 as clock source
            IFS0bits.IC1IF     = 0; // Clear Interrupt Flag
            IEC0bits.IC1IE     = 1; // Enable Interrupt
            break;
            
        case IC_2:
            
            IC2CON1bits.ICM    = 0b001; // Capture mode : every edge
            IFS0bits.IC2IF     = 0; // Clear Interrupt Flag
            IEC0bits.IC2IE     = 1; // Enable Interrupt
            break;
            
        case IC_3:
            
            IC3CON1bits.ICM    = 0b001; // Capture mode : every edge
            IFS2bits.IC3IF     = 0; // Clear Interrupt Flag
            IEC2bits.IC3IE     = 1; // Enable Interrupt
            break;
            
        case IC_4:
            
            IC4CON1bits.ICM    = 0b001; // Capture mode : every edge
            IFS2bits.IC4IF     = 0; // Clear Interrupt Flag
            IEC2bits.IC4IE     = 1; // Enable Interrupt
            break;
            
        case IC_5:
            
            IC5CON1bits.ICM    = 0b001; // Capture mode : every edge
            IFS2bits.IC5IF     = 0; // Clear Interrupt Flag
            IEC2bits.IC5IE     = 1; // Enable Interrupt
            break;
            
        case IC_6:
            
            IC6CON1bits.ICM    = 0b001; // Capture mode : every edge
            IFS2bits.IC6IF     = 0; // Clear Interrupt Flag
            IEC2bits.IC6IE     = 1; // Enable Interrupt
            break;
            
        case IC_7:
            
            IC7CON1bits.ICM    = 0b001; // Capture mode : every edge
            IFS1bits.IC7IF     = 0; // Clear Interrupt Flag
            IEC1bits.IC7IE     = 1; // Enable Interrupt
            break;
            
        case IC_8:
            
            IC8CON1bits.ICM    = 0b001; // Capture mode : every edge
            IFS1bits.IC8IF     = 0; // Clear Interrupt Flag
            IEC1bits.IC8IE     = 1; // Enable Interrupt
            break;
            
        case IC_9:
            
            IC9CON1bits.ICM    = 0b001; // Capture mode : every edge
            IFS5bits.IC9IF     = 0; // Clear Interrupt Flag
            IEC5bits.IC9IE     = 1; // Enable Interrupt
            break;
            
        case IC_10:
            
            IC10CON1bits.ICM    = 0b001; // Capture mode : every edge
            IFS7bits.IC10IF     = 0; // Clear Interrupt Flag
            IEC7bits.IC10IE     = 1; // Enable Interrupt
            break;
            
        case IC_11:
            
            IC11CON1bits.ICM    = 0b001; // Capture mode : every edge
            IFS7bits.IC11IF     = 0; // Clear Interrupt Flag
            IEC7bits.IC11IE     = 1; // Enable Interrupt
            break;
            
        case IC_12:
            
            IC12CON1bits.ICM    = 0b001; // Capture mode : every edge
            IFS8bits.IC12IF     = 0; // Clear Interrupt Flag
            IEC8bits.IC12IE     = 1; // Enable Interrupt
            break;
            
        default:
            // error
            break;
    }
}

void odometer_init(e_odometers odometer_id) {
    
    switch (odometer_id) {
        
        case ODOMETER_FRONT_LEFT:
        
            ic_init(IC_7);
            ic_init(IC_8);
            ic_init(IC_9);
            odometers.front_left      = 0;
            odometers.prev_front_left = 0;
            break;
            
        case ODOMETER_FRONT_RIGHT:
            
            ic_init(IC_4);
            ic_init(IC_5);
            ic_init(IC_6);
            odometers.front_right      = 0;
            odometers.prev_front_right = 0;
            break;

        case ODOMETER_BACK_LEFT:
            
            ic_init(IC_10);
            ic_init(IC_11);
            ic_init(IC_12);
            odometers.back_left      = 0;
            odometers.prev_back_left = 0;
            break;
            
        case ODOMETER_BACK_RIGHT:
            
            ic_init(IC_1);
            ic_init(IC_2);
            ic_init(IC_3);
            odometers.back_right      = 0;
            odometers.prev_back_right = 0;
            break;
            
        default:
            // error
            break;
    }
}

u8 hall_to_dir(u8 current, u8 prev) {
    
    u8 odo_prev[] = {1, 3, 2, 6, 4, 5};
    u8 odo_curr[] = {5, 1, 3, 2, 6, 4};
    u8 odo_next[] = {4, 5, 1, 3, 2, 6};
    
    u8 i;
    
    // Searching for previous coder position ...
    for (i=0; (prev != odo_curr[i]) && (i < sizeof(odo_curr)) ;i++) ;
    
    // error
    if (i == sizeof(odo_curr))
        return 2;
    
    // coder is decreasing
    if (current == odo_prev[i])
        return 0;
    
    // code is increasing
    if (current == odo_next[i])
        return 1;
    
    // error
    return 2;
}

void odometry_update(e_odometers odometer_id) {
    
    u8 hallValue, hallDir;
    
    switch (odometer_id) {
        
        case ODOMETER_FRONT_LEFT:
            
            hallValue = (u8) (PORTFbits.RF2 | PORTFbits.RF5 <<1 | PORTFbits.RF3 <<2); // Read hall
            hallDir = hall_to_dir(hallValue, odometers.odo_prev_fl);
            
            if (hallDir == 0)
                odometers.front_left--;
            else if (hallDir == 1)
                odometers.front_left++;
    
            odometers.odo_prev_fl = hallValue;
        
            break;
            
        case ODOMETER_FRONT_RIGHT:
            
            hallValue = (u8) ((PORTF & 0x3) | PORTFbits.RF4 <<2); // Read hall
            hallDir = hall_to_dir(hallValue, odometers.odo_prev_fr);
            
            if (hallDir == 0)
                odometers.front_right--;
            else if (hallDir == 1)
                odometers.front_right++;
    
            odometers.odo_prev_fr = hallValue;
        
            break;
            
        case ODOMETER_BACK_LEFT:
            
            hallValue = (u8) ((PORTD) & 0x0007); // Read hall
            hallDir = hall_to_dir(hallValue, odometers.odo_prev_bl);
            
            if (hallDir == 0)
                odometers.back_left--;
            else if (hallDir == 1)
                odometers.back_left++;
    
            odometers.odo_prev_bl = hallValue;
        
            break;
            
        case ODOMETER_BACK_RIGHT:
            
            hallValue = (u8) ((PORTD >>3) & 0x0007); // Read hall
            hallDir = hall_to_dir(hallValue, odometers.odo_prev_br);
            
            if (hallDir == 0)
                odometers.back_right--;
            else if (hallDir == 1)
                odometers.back_right++;
    
            odometers.odo_prev_br = hallValue;
        
            break;
    }
}

void speed_calculation(e_odometers odometer_id) {
    
    switch (odometer_id) {
        
        case ODOMETER_FRONT_LEFT:
            
            odometers.speed_front_left = odometers.front_left - odometers.prev_front_left;
            odometers.prev_front_left = odometers.front_left;
            
            pid.fooPID[2].measuredOutput = Q15((float) odometers.speed_front_left / 100.f);
            
            break;
            
        case ODOMETER_FRONT_RIGHT:
            
            odometers.speed_front_right = odometers.front_right - odometers.prev_front_right;
            odometers.prev_front_right = odometers.front_right;
            
            pid.fooPID[3].measuredOutput = Q15((float) -odometers.speed_front_right / 100.f);
            break;
            
        case ODOMETER_BACK_LEFT:
            
            odometers.speed_back_left = odometers.back_left - odometers.prev_back_left;
            odometers.prev_back_left = odometers.back_left;
            
            pid.fooPID[0].measuredOutput = Q15((float) odometers.speed_back_left / 100.f);
            break;
            
        case ODOMETER_BACK_RIGHT:
            
            odometers.speed_back_right = odometers.back_right - odometers.prev_back_right;
            odometers.prev_back_right = odometers.back_right;
            
            pid.fooPID[1].measuredOutput = Q15((float) -odometers.speed_back_right / 100.f);
            break;
    }
}

// Interrupts

void __attribute__((interrupt, no_auto_psv)) _IC1Interrupt(void) {
    
    IFS0bits.IC1IF = 0; // Clear interrupt flag
    odometry_update(ODOMETER_BACK_LEFT);
}

void __attribute__((interrupt, no_auto_psv)) _IC2Interrupt(void) {
    
    IFS0bits.IC2IF = 0; // Clear interrupt flag
    odometry_update(ODOMETER_BACK_LEFT);
}

void __attribute__((interrupt, no_auto_psv)) _IC3Interrupt(void) {
    
    IFS2bits.IC3IF = 0; // Clear interrupt flag
    odometry_update(ODOMETER_BACK_LEFT);
}

// ---

void __attribute__((interrupt, no_auto_psv)) _IC4Interrupt(void) {
    
    IFS2bits.IC4IF = 0; // Clear interrupt flag
    odometry_update(ODOMETER_BACK_RIGHT);
}

void __attribute__((interrupt, no_auto_psv)) _IC5Interrupt(void) {
    
    IFS2bits.IC5IF = 0; // Clear interrupt flag
    odometry_update(ODOMETER_BACK_RIGHT);
}

void __attribute__((interrupt, no_auto_psv)) _IC6Interrupt(void) {
    
    IFS2bits.IC6IF = 0; // Clear interrupt flag
    odometry_update(ODOMETER_BACK_RIGHT);
}

// ---

void __attribute__((interrupt, no_auto_psv)) _IC7Interrupt(void) {
    
    IFS1bits.IC7IF = 0; // Clear interrupt flag
    odometry_update(ODOMETER_FRONT_LEFT);
}

void __attribute__((interrupt, no_auto_psv)) _IC8Interrupt(void) {
    
    IFS1bits.IC8IF = 0; // Clear interrupt flag
    odometry_update(ODOMETER_FRONT_LEFT);
}

void __attribute__((interrupt, no_auto_psv)) _IC9Interrupt(void) {
    
    IFS5bits.IC9IF = 0; // Clear interrupt flag
    odometry_update(ODOMETER_FRONT_LEFT);
}

// ---

void __attribute__((interrupt, no_auto_psv)) _IC10Interrupt(void) {
    
    IFS7bits.IC10IF = 0; // Clear interrupt flag
    odometry_update(ODOMETER_FRONT_RIGHT);
}

void __attribute__((interrupt, no_auto_psv)) _IC11Interrupt(void) {
    
    IFS7bits.IC11IF = 0; // Clear interrupt flag
    odometry_update(ODOMETER_FRONT_RIGHT);
}

void __attribute__((interrupt, no_auto_psv)) _IC12Interrupt(void) {
    
    IFS8bits.IC12IF = 0; // Clear interrupt flag
    odometry_update(ODOMETER_FRONT_RIGHT);
}
